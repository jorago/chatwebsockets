const express = require('express');
const path = require('path');

const app = express();

const { Server } = require('socket.io');

//settings
app.set('port', process.env.PORT || 3000);

//static files
app.use(express.static(path.join(__dirname, 'src/public')));

//start server
const server = app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});


const io = new Server(server);

//websockets
io.on('connection', (socket) => {
    console.log('New connection', socket.id);

    socket.on('chat:message', (data) => {
        io.sockets.emit('chat:message', data);
    });
    
    socket.on('chat:typing', (data) => {
        socket.broadcast.emit('chat:typing', data);
    });
});
